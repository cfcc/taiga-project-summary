#!/bin/bash

USAGE='get-picture.h [-e EMPNUM] [USERNAME]'

while getopts "he:" opt; do
    case ${opt} in
	h)
	    echo ${USAGE}
	    exit 1
	    ;;
	e)
	    EMPNUM=${OPTARG}
	    ;;
	\?)
	    echo ${USAGE}
	    exit 1
	    ;;
    esac
done

shift $((OPTIND -1))

if [[ "z" = "z${EMPNUM}" ]]
then
    if [[ "z" = "z$1" ]]
    then
	echo -n "Enter Username: "
	read USERNAME
    else
	USERNAME=$1
    fi
    EMPNUM=`get-aduser.sh -u $USERNAME | grep employeeNumber | awk ' { print $2; } '`
    if [[ "z" = "z${EMPNUM}" ]]
    then
	echo "Error searching for ${USERNAME}"
	exit 1
    fi
fi

if [[ "z" != "z${EMPNUM}" ]]
then
    wget http://bbtsdbcape:8080/images/$EMPNUM.jpg
else
    echo "ERROR: employee number blank"
    exit 1
fi
