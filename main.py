import sys
from getpass import getpass
import configparser
from taiga import TaigaAPI
import argparse

TMPL_HTML_HEADER = """
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <style>
            table { border-collapse: collapse; width: 100%; }
            th, td { padding: 0 4px; border: 1px solid #ddd; }
            tr:nth-child(even) { background-color: #f2f2f2; }
            th { padding-top: 12px; padding-bottom: 4px; text-align: left; }

            .blocked { color: red; padding-left: 1em; }
            .indent { padding-left: 2em; }
            .story { font-size: smaller; }
            .story-blocked { color: red; }
            .story-indent { padding-left: 1em; }
            .task { font-size: smaller; }
            .task-indent { padding-left: 3em; }

            @page { 0.5cm; }
            body {
                font: 12pt Georgia, "Times New Roman", Times, serif;
                line-height: 1.3;
            }
        </style>
    </head>
    <body>
    <h1>Project List</h1>
"""
TMPL_EPIC_HEADER = """<tr>
    <th width=\"50%\">Description</th>
    <th width=\"25%\">Assigned</th>
    <th width=\"25%\">Status</th>
"""
TMPL_EPIC = """<tr>
  <td>{0}</td>
  <td>{2}</td>
  <td><span style=\"color: {3};\">{1}</span></td>
</tr>"""
TMPL_BLOCKED = """<tr>
  <td><span class=\"blocked\">BLOCKED</span>: {}</td>
  <td colspan=\"2\"></td>
</tr>"""
TMPL_STORY_LINE = """<tr class=\"story\">
  <td class=\"indent\">{0}</td>
  <td class=\"story-indent\">{2}</td>
  <td class=\"story-indent\">{1}</td>
</tr>"""
TMPL_TASK_LINE = """<tr class=\"task\">
  <td class=\"task-indent\">{0}</td>
  <td class=\"indent\">{2}</td>
  <td class=\"indent\">{1}</td>
</tr>"""


def print_progress(iteration, total, prefix='', suffix='', decimals=1, bar_length=100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        bar_length  - Optional  : character length of bar (Int)

    From: https://gist.github.com/aubricus/f91fb55dc6ba5557fbab06119420dd6a
    """
    percents = f'{100 * (iteration / float(total)):.2f}'
    # str_format = "{0:." + str(decimals) + "f}"
    # percents = str_format.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    bar = f'{"█" * filled_length}{"-" * (bar_length - filled_length)}'

    sys.stdout.write(f'\r{prefix} |{bar}| {percents}% {suffix}')
    # '\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)

    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()


def list_projects(api):
    line_fmt = "{: 4d}    {}"
    print("\n")
    print("ID..    Slug...........")
    for p in api.projects.list():
        print(line_fmt.format(p.id, p.slug))
    print("\n")


def generate_report(api, project_id, show_epics, show_stories, show_tasks, filename):
    report = [TMPL_HTML_HEADER]

    if project_id is None:
        projects = api.projects.list()
    else:
        projects = [api.projects.get(project_id)]

    total = len(projects)
    i = 0
    for p in projects:
        i += 1
        print_progress(i, total, bar_length=60)
        if p.i_am_member or p.i_am_owner:
            if p.is_epics_activated or show_epics:
                title = "<h2 data-id='{}'>Project: {}</h2>".format(p.id, p)
                report.append(title)
                report.append("")
            if p.is_epics_activated:
                rr = api.raw_request.get('epics?project={}'.format(p.id))
                report.append("<table>")
                report.append(TMPL_EPIC_HEADER)
                for epic_json in rr.json():
                    assigned_to = ""
                    if 'assigned_to_extra_info' in epic_json:
                        try:
                            assigned_to = epic_json['assigned_to_extra_info']['full_name_display']
                        except TypeError:
                            pass
                    report.append(TMPL_EPIC.format(epic_json['subject'],
                                                   epic_json['status_extra_info']['name'],
                                                   assigned_to,
                                                   epic_json['status_extra_info']['color']
                                                   ))
                    if epic_json['is_blocked']:
                        report.append(TMPL_BLOCKED.format(epic_json['blocked_note']))
                    if show_stories:
                        rr_us = api.raw_request.get('epics/{}/related_userstories'.format(epic_json['id']))
                        for rec in sorted(rr_us.json(), key=lambda x: x['order']):
                            user_story = api.user_stories.get(rec['user_story'])
                            try:
                                us_assigned = user_story.assigned_to_extra_info['full_name_display']
                            except (AttributeError, TypeError):
                                us_assigned = ''
                            if user_story.is_blocked:
                                status_msg = "<span class=\"story-blocked\">BLOCKED</span>: {0}".format(user_story.blocked_note)
                            else:
                                status_msg = user_story.status_extra_info['name']

                            if user_story.due_date:
                                status_msg += " (Due: {})".format(user_story.due_date)

                            report.append(TMPL_STORY_LINE.format(user_story.subject,
                                                                 status_msg,
                                                                 us_assigned))
                            if show_tasks:
                                for task in sorted(user_story.list_tasks(), key=lambda x: x.us_order):
                                    if task.status is not None:
                                        this_status = task.status_extra_info['name']
                                    else:
                                        this_status = ""
                                    if task.assigned_to_extra_info is not None:
                                        task_assigned = task.assigned_to_extra_info['full_name_display']
                                    else:
                                        task_assigned = ""
                                    if task.due_date:
                                        this_status += " (Due: {})".format(task.due_date)
                                    report.append(TMPL_TASK_LINE.format(task.subject,
                                                                        this_status,
                                                                        task_assigned))
                report.append("</table>")
            report.append("")

    report.append("</body></html>")

    if filename:
        with open(filename, 'w') as fd:
            for line in report:
                fd.write(line + "\n")
    else:
        for line in report:
            print(line)


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("-a", "--all", action="store_true", help="Show all projects, even without Epics")
    parser.add_argument("-s", "--stories", action="store_true", help="Show user stories")
    parser.add_argument("-t", "--tasks", action="store_true", help="Show tasks for each story")
    parser.add_argument("-w", "--write", help="Write report to the given file")
    parser.add_argument("-p", "--project", help="Select a project by ID", default=None)
    parser.add_argument("-l", "--list", action="store_true", help="List project IDs")

    args = parser.parse_args()

    config = configparser.ConfigParser()
    config.read("config.ini")

    if args.all:
        config['PROJECT']['SHOW_WITHOUT_EPICS'] = 'yes'

    if args.stories:
        config['PROJECT']['SHOW_STORIES'] = 'yes'

    api = TaigaAPI(
        host=config['DEFAULT']['HOST'],
        auth_type=config['DEFAULT']['AUTH_TYPE']
    )

    api.auth(
        username=input("Enter username: "),
        password=getpass("Enter password: ")
    )

    if args.list:
        list_projects(api)
    else:
        generate_report(api,
                        args.project,
                        config['PROJECT'].getboolean('SHOW_WITHOUT_EPICS', fallback=False),
                        config['PROJECT'].getboolean('SHOW_STORIES', fallback=False),
                        args.tasks,
                        args.write)


if __name__ == '__main__':
    main()
