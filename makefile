.PHONY: all clean list summary 2020
PYTHON=./venv/bin/python

all:
	$(PYTHON) main.py -s -w report.html
	/usr/bin/google-chrome --new-window report.html
#	pandoc -s -o report.odt -f html -t odt report.html
#	oowriter report.odt

list:
	$(PYTHON) main.py --list

summary:
	$(PYTHON) main.py -w report.html
	/usr/bin/google-chrome --new-window report.html

2020:
	$(PYTHON) main.py -s -p 28 -w report.html
	/usr/bin/google-chrome --new-window report.html

clean:
	-rm report.*
